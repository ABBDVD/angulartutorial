/*global angular */

/**
 * The main controller for the app. The controller:
 * - retrieves and persists the model via the todoStorage service
 * - exposes the model to the template and provides event handlers
 */
angular.module('todomvc')
		.controller('TodoCtrl', function TodoCtrl($scope, $routeParams, $filter, $interval, store) {
			'use strict';
			var todos = $scope.todos = store.todos;
			var running = $scope.running = {};

			$scope.newTodo = '';
			$scope.editedTodo = null;

			//Here we watch for changes on todos, so if it changes,	we update come counts on it.
			$scope.$watch('todos', function () {
				$scope.remainingCount = $filter('filter')(todos, { completed: false }).length;
				$scope.completedCount = todos.length - $scope.remainingCount;
				$scope.allChecked = !$scope.remainingCount;
			}, true);

			// Monitor the current route for changes and adjust the filter accordingly.
			$scope.$on('$routeChangeSuccess', function () {
				var status = $scope.status = $routeParams.status || '';

				$scope.statusFilter = (status === 'active') ?
				{ completed: false } : (status === 'completed') ?
				{ completed: true } : null;
			});

			$scope.addTodo = function () {
				var newTodo = {
					subject: $scope.newTodo.trim(),
					completed: false,
					last: 0,
					spent: 0,
					total: 0
				};

				if (newTodo.subject === "") {
					return;
				}

				$scope.saving = true;
				store.create(newTodo)
						.then(function success() {
							$scope.newTodo = '';
						})
						.finally(function () {
							$scope.saving = false;
						});
			};

			$scope.editTodo = function (todo) {
				$scope.editedTodo = todo;
				// Clone the original todo to restore it on demand, if edit is canceled.
				$scope.originalTodo = angular.extend({}, todo);
			};

			$scope.saveEdits = function (todo, event) {
				// Blur events are automatically triggered after the form submit event.
				// This does some unfortunate logic handling to prevent saving twice.
				if (event === 'blur' && $scope.saveEvent === 'submit') {
					$scope.saveEvent = null;
					return;
				}

				$scope.saveEvent = event;

				if ($scope.reverted) {
					// Todo edits were reverted-- don't save.
					$scope.reverted = null;
					return;
				}

				todo.subject = todo.subject.trim();

				if (todo.subject === $scope.originalTodo.subject) {
					return;
				}

				store[todo.subject ? 'put' : 'delete'](todo)
						.then(function success() {
						}, function error() {
							todo.subject = $scope.originalTodo.subject;
						})
						.finally(function () {
							$scope.editedTodo = null;
						});
			};

			$scope.revertEdits = function (todo) {
				todos[todos.indexOf(todo)] = $scope.originalTodo;
				$scope.editedTodo = null;
				$scope.originalTodo = null;
				$scope.reverted = true;
			};

			$scope.removeTodo = function (todo) {
				store.delete(todo);
			};

			$scope.saveTodo = function (todo) {
				store.put(todo);
			};

			$scope.toggleCompleted = function (todo, completed) {
				if (angular.isDefined(completed)) {
					todo.completed = completed;
				}
				store.put(todo, todos.indexOf(todo))
						.then(function success() {
						}, function error() {
							todo.completed = !todo.completed;
						});
			};

			// we just hand on the function reference.
			$scope.clearCompletedTodos = store.clearCompleted;

			$scope.markAll = function (completed) {
				todos.forEach(function (todo) {
					if (todo.completed !== completed) {
						$scope.toggleCompleted(todo, completed);
					}
				});
			};
			
			$scope.login = function () {
				store.login("Chris")
					.then(function ret(success) {
						$scope.loggedin = success;
					});
			};
			
			$scope.logout = function () {
				store.logout()
					.then(function ret(success) {
						$scope.loggedin = success;
					});
			};
			
			$scope.startTimer = function(todo) {
					
				if (!_.has($scope.running, todo.id)) {
					
					var tmpTime = {};
					todo.last += 1;
					tmpTime[todo.id] =  {
							"num":todo.last,
							"start":new Date()
							};
					running[todo.id]=tmpTime;
				};
				
				};
			
			$scope.stopTimer = function(todo) {
				if (_.has($scope.running, todo.id)) {
					tmpTime = running[todo.id];
					delete running[todo.id];
					time = {
						"id":tmpTime.num,
						"start":tmpTime.start.toISOString(),
						"end":new Date().toISOString()
						};
					store.putTime(todo.id, time);
				};
			};
			
			$scope.timing = function(todo) {
				return _.has($scope.running, todo.id);
			}
			$interval(function() {
				_.each($scope.running,
					function(value, key, list) {
						console.log(key);
							var todo = _.find(todos, function(todo) {
						console.log(todo);
								return todo.id===key;
							});
							console.log(todo);
							if (todo) {
							todo.total = todo.spent + new Date() - value.start;
							};
					});
				}, 500);
		});
